package polymorphism;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {
    @Test
    public void getAuthorReturnsJohnDoe(){
        Book b = new Book("Java the Book", "John Doe");
        assertEquals("Author should be John Doe", "John Doe", b.getAuthor());
    }

    @Test
    public void getTitleReturnsJavaTheBook(){
        Book b = new Book("Java the Book", "John Doe");
        assertEquals("Title should be Java the Book", "Java the Book", b.getTitle());
    }

    @Test
    public void toStringReturnsJavaTheBookByJohnDoe(){
        Book b = new Book("Java the Book", "John Doe");
        assertEquals("Should return Java the Book by John Doe", "Java the Book by John Doe", b.toString());
    }
}
