package polymorphism;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ElectronicBookTest {
    @Test
    public void getNumberBytesReturn314(){
        ElectronicBook eb = new ElectronicBook("Java the Book", "John Doe", 314);
        assertEquals("Should return 314", 314, eb.getNumberBytes());
    }

    @Test
    public void getAuthorReturnsJohnDoe(){
        ElectronicBook b = new ElectronicBook("Java the Book", "John Doe", 314);
        assertEquals("Author should be John Doe", "John Doe", b.getAuthor());
    }

    @Test
    public void getTitleReturnsJavaTheBook(){
        ElectronicBook eb = new ElectronicBook("Java the Book", "John Doe", 314);
        assertEquals("Title should be Java the Book", "Java the Book", eb.getTitle());
    }

    @Test
    public void toStringReturnsJavaTheBookByJohnDoe(){
        ElectronicBook eb = new ElectronicBook("Java the Book", "John Doe", 314);
        assertEquals("Should return Java the Book by John Doe Size: 314b", "Java the Book by John Doe Size: 314b", eb.toString());
    }
}
