package polymorphism;

public class ElectronicBook extends Book{
    private int numberBytes;

    public int getNumberBytes() {
        return numberBytes;
    }

    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public String toString(){
        return super.toString() + " Size: " + this.numberBytes + "b";
    }
}
