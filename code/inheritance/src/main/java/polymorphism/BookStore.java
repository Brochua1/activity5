package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("Axel's autobiography", "Axel");
        books[1] = new ElectronicBook("Axel's Electronic Biography", "Axel", 4706);
        books[2] = new Book("Java the Book", "John Doe");
        books[3] = new ElectronicBook("The Player's Guide to Dungeons and Dragons", "Wizards of The Coast", 79805);
        books[4] = new ElectronicBook("The Book of All Time", "Joshua Barns", 7913);

        ElectronicBook eb = (ElectronicBook)books[0];
        System.out.println(eb.getNumberBytes());

        for (Book b : books)
            System.out.println(b);
    }
}
